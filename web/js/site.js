$(document).ready(function () {

    var radion = $('#radios').radiosToSlider({
        animation: true,
    });

    $('#radios').on('radiochange',function ($levels, $inputs) {
        var color = $('input[type=radio]:checked').val();
        $('.slider-bar.transition-enabled,.slider-knob').css({
            'background-color':color,
            'opacity': '.3'
        })
    });

    $('body').on('beforeSubmit', 'form#dynamic-form', function () {
        var form = $(this);
        // return false if form still have some validation errors
        if (form.find('.has-error').length) {
            return false;
        }
        // submit form
        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                $('.result').text(response);
            },
            error: function () {
                console.log('internal server error');
            }
        });
        $(this)[0].reset();
        $('ins[data-radio=option5]').click();
        return false;
    });

});