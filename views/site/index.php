<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?php
    $form = \yii\widgets\ActiveForm::begin([
        'id' => 'dynamic-form',
        'options' => ['class' => 'form-horizontal'],
        'enableAjaxValidation' => true,
        'validationUrl' => '?r=test/validation-url',
        'action' => '?r=test/save-data'
    ]) ?>
    <div id="radios">
        <input id="option1" name="Test[color]" value="green" type="radio">
        <label for="option1">1</label>
        <input id="option2" name="Test[color]" value="green" type="radio">
        <label for="option2">2</label>
        <input id="option3" name="Test[color]" value="green" type="radio">
        <label for="option3">3</label>
        <input id="option4" name="Test[color]" value="yellow" type="radio">
        <label for="option4">4</label>
        <input id="option5" name="Test[color]" value="yellow" type="radio" checked>
        <label for="option5">5</label>
        <input id="option6" name="Test[color]" value="yellow" type="radio">
        <label for="option6">6</label>
        <input id="option7" name="Test[color]" value="red" type="radio">
        <label for="option7">7</label>
        <input id="option8" name="Test[color]" value="red" type="radio">
        <label for="option8">8</label>
        <input id="option9" name="Test[color]" value="black" type="radio">
        <label for="option9">9</label>
        <input id="option10" name="Test[color]" value="black" type="radio">
        <label for="option10">10</label>
    </div>

    <?= $form->field($model, 'email')->textInput([
        'type' => 'email'
    ]) ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <div class="form-group">
        <div class="col-lg-2 save-button">
            <?= \yii\helpers\Html::submitButton('Добавить', ['class' => 'btn btn-primary btn-block']) ?>
        </div>
    </div>
    <?php \yii\widgets\ActiveForm::end() ?>
    <div class="result"></div>
</div>