<?php

use GuzzleHttp\Client;

class SaveDataTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    /**
     *
     *@dataProvider setData
     */
    public function testSomeFeature($color,$email,$name)
    {
        $client = new Client([
            'base_uri' => Yii::$app->params['url'],
        ]);

        $response = $client->post('index-test.php?r=test/save-data',[
            'form_params' => [
                'Test'=>[
                    'color' => $color,
                    'email' => $email,
                    'name' => $name
                ]
            ]
        ]);

        $this->assertContains($response->getBody()->getContents(),['True','False']);
    }

    public function setData(){
        return [
            [
                'color' => 'red',
                'email'=> 'red@red.com',
                'name'=> 'redName'
            ],[
                'color' => 'yellow',
                'email'=> 'red.com',
                'name'=> 'redName'
            ], [
                'color' => 'blue',
                'email'=> 'red@red.com',
                'name'=> 'НА на'
            ], [
                'color' => '',
                'email'=> 'red@red.com',
                'name'=> 'redName'
            ]
        ];
    }

}