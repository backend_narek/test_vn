<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Test extends ActiveRecord
{
    /**
     * @return string название таблицы, сопоставленной с этим ActiveRecord-классом.
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['color', 'email','name'], 'required'],
            [['email','name'],'string','max'=>50,'min'=>3],
            ['email', 'email'],
            ['name','match','pattern' => '/^[a-zA-Z\s]+$/',]
        ];
    }
}
