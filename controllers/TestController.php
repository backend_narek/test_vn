<?php
/**
 * Created by PhpStorm.
 * User: narek
 * Date: 07.02.2020
 * Time: 11:28
 */

namespace app\controllers;

use app\models\Test;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;


class TestController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'save' => ['post'],
                ],
            ],
        ];
    }

    public function actionValidationUrl(){
        $model = new Test();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionSaveData(){
        $request = \Yii::$app->request->post();
        $model = new Test();

        $model->load($request);
        if($model->validate()){
            \Yii::$app->db->createCommand("CALL sp_SaveData(:color, :email,:name,@res)")
                ->bindValue(':color' , $request['Test']['color'] )
                ->bindValue(':email', $request['Test']['email'])
                ->bindValue(':name', $request['Test']['name'])
                ->queryAll();
            $result = \Yii::$app->db->createCommand("SELECT @res as count")->queryAll();

            return (int)$result[0]['count'] ? 'True' : 'False';
        }

    }

}